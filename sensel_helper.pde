import java.util.List;
import java.nio.charset.StandardCharsets; 

//Holds x and y coordinates
public class Coor
{
  int x; int y;
  public Coor(){x=0;y=0;}
  public Coor(int _x, int _y){x=_x;y=_y;}
  public void setCoor(int x, int y){this.x=x;this.y=y;};
}

int default_height = 75;
int default_width  = 75;
color default_color = color(100);
color default_color_highlight  = color(51);

//Holder of the Button
public class Button
{
  //private ArrayList<Coor> coor;  // the points of the area
  Coor coor = new Coor(); 
  int width = default_width;
  int height = default_height;
  color col = default_color;
  color col_highlight = default_color_highlight;
  boolean hover_over = false;
  boolean is_selected = false;
  int sid = -1; 
  private String name = "default";  //name of input (id, index, idk)
  private String type = "button";   //type of input (button, key, dial, pad, etc)
  private String value;              //the type of value it raises ("DELETE", touch w/ coordinates, or Rotation)
  
  public Button() {}
  public Button(String name, int _x, int _y) 
  {
    this.coor = new Coor(_x,_y);
    this.name = name;
  }
  public Button(String name, int _x, int _y, int width, int height) 
  {
    this.coor = new Coor(_x,_y);
    this.width = width; 
    this.height = height;
    this.name = name;
  }
  public Button(String name, String type, String value, int _x, int _y, int width, int height) 
  {
    this.coor = new Coor(_x,_y);
    this.width = width; 
    this.height = height;
    this.name = name;
    this.type = type; 
    this.value = value; 
  }
  public void set_coor(int _x, int _y)  
  {
    if (_x-width/2 < 0)                          _x = width/2; 
    else if (_x+width/2 > OVERLAYER_WIDTH_PX)    _x = OVERLAYER_WIDTH_PX-width/2; 
    
    if (_y-height/2 < 0)                         _y = height/2;
    else if (_y+height/2 > OVERLAYER_HEIGHT_PX)  _y = OVERLAYER_HEIGHT_PX-height/2;
    
    coor.setCoor(_x-width/2,_y-height/2);
  }
  public void set_name(String _name)  {name=_name;}
  public void set_type(String _type)  {type=_type;}
  public void set_val (String _val)   {value=_val;}
  public void set_selected(boolean _s)    {is_selected = _s;} 
  public boolean is_hover_over(int x, int y)  
  {
    return (x >= this.coor.x && x <= this.coor.x+this.width && 
            y >= this.coor.y && y <= this.coor.y+this.height);
  }
  public void display()
  {
    stroke(255);
    if (this.hover_over) {fill(col_highlight);}
    else {fill(col);}
    rect(this.coor.x, this.coor.y, this.width, this.height);
    fill(#86E2D5);
    text(this.name,this.coor.x,this.coor.y+this.height/2);
  }
}

public class Overlay
{
  private String name;                //name of Overlay
  private ArrayList<Button> buttons;  //the buttons of the Overlay
  
  public Overlay() 
  {
    name = "default";
    buttons = new ArrayList<Button>();
  }
  
  public void addButton(Button butt) {buttons.add(butt);}
  public void remButton(Button butt) {if(buttons.contains(butt))buttons.remove(buttons.indexOf(butt));}
  
  public void loadOverlay(File file)
  {
    println("LOADING");
    try
    {
      List<String> lines = Files.readAllLines(file.toPath(), StandardCharsets.UTF_8);
      for (int i = 1; i < lines.size(); ++i)
      {
        String line = lines.get(i); 
        println(line);
        String[] column = line.split("\t");
        Button b = new Button(column[0], column[1], column[2], Integer.parseInt(column[3]), Integer.parseInt(column[4]), Integer.parseInt(column[5]), Integer.parseInt(column[6]));
        addButton(b); 
      }
    }
    catch(IOException e){};
  }
}
