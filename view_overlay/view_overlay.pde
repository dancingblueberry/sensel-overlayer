/**
 * Read Contacts
 * by Aaron Zarraga - Sensel, Inc
 * 
 * This opens a Sensel sensor, reads contact data, and prints the data to the console.
 */

import java.util.Map;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.awt.Robot;
import javax.swing.KeyStroke;
import java.awt.AWTException;

//We will scale the height such that we get the same aspect ratio as the sensor
int WINDOW_WIDTH_PX = 1150;
int WINDOW_HEIGHT_PX = 600;
int OVERLAYER_WIDTH_PX = int (WINDOW_WIDTH_PX * 0.90);
int OVERLAYER_HEIGHT_PX = int (WINDOW_HEIGHT_PX * 0.90);

boolean sensel_sensor_opened = false;
boolean selecting = false;
boolean holding   = false;
String  uInput    = "";
HashMap<Integer, Button> selected_buttons;
ArrayList<Button> gui_buttons;

PFont font, fontLarge;
color rectColor;
color rectHighlight;

SenselDevice sensel;
Overlay overlay;
Robot r;

void setup() 
{
  size(1150, 600);
  DisposeHandler dh = new DisposeHandler(this);
  sensel = new SenselDevice(this);
  selected_buttons = new HashMap<Integer, Button>();  
  overlay = new Overlay(); 
  File file = new File("Documents/git/sensel_overlayer/view_overlay/overlays/overlay1");
  overlay.loadOverlay(file); 
  sensel_sensor_opened = sensel.openConnection();
  
  try {r = new Robot();}
  catch (AWTException e) {};

  font = createFont("Arial", 12, true);
  fontLarge = createFont("Arial", 30, true);

  if (!sensel_sensor_opened)
  {
    println("Unable to open Sensel sensor!");
    exit();
    return;
  }

  //Init window height so that display window aspect ratio matches sensor.
  //NOTE: This must be done AFTER senselInit() is called, because senselInit() initializes
  //  the sensor height/width fields. This dependency needs to be fixed in later revisions 
  //WINDOW_HEIGHT_PX = (int) (sensel.getSensorHeightMM() / sensel.getSensorWidthMM() * WINDOW_WIDTH_PX);

  //Enable contact sending
  sensel.setFrameContentControl(SenselDevice.SENSEL_FRAME_CONTACTS_FLAG);

  //Enable scanning
  sensel.startScanning();
}

//show the magic touch
void DisplayTouch(SenselContact[] c)
{
  //show sensor inputs
  for (int i = 0; i < c.length; i++)
  {
    int force = c[i].total_force;

    float sensor_x_mm = c[i].x_pos_mm;
    float sensor_y_mm = c[i].y_pos_mm;

    int screen_x = (int) (c[i].x_pos_mm / sensel.getSensorWidthMM()  * WINDOW_WIDTH_PX);
    int screen_y = (int) (c[i].y_pos_mm / sensel.getSensorHeightMM() * WINDOW_HEIGHT_PX);

    int id = c[i].id;
    int event_type = c[i].type;

    String event;
    switch (event_type)
    {
      case SenselDevice.SENSEL_EVENT_CONTACT_INVALID:
        event = "invalid"; 
        break;
      case SenselDevice.SENSEL_EVENT_CONTACT_START:
        event = "start";
        break;
      case SenselDevice.SENSEL_EVENT_CONTACT_MOVE:
        event = "move";
        break;
      case SenselDevice.SENSEL_EVENT_CONTACT_END:
        event = "end";
        if (selected_buttons.get(c[i].id) != null && !holding)
        {
          selected_buttons.remove(c[i].id);
        }
        break;
      default:
        event = "error";
    }

    println("Contact ID " + id + ", event=" + event + ", mm coord: (" + sensor_x_mm + ", " + sensor_y_mm + "), force=" + force); 

    int size = force / 100;
    if (size < 10) {size = 10;}

    //update(screen_x, screen_y);
    ellipse(screen_x, screen_y, size, size);
  }
}

void UpdateOverlay(SenselContact[] c) {
  //println("Displaying Overlay");
  selecting = false;
  for (int i = 0; i < overlay.buttons.size (); ++i)
  {
    Button b = overlay.buttons.get(i);
    boolean temp_hover_over = false;
    for (int j = 0; j < c.length; ++j)
    {
      int screen_x = (int) (c[j].x_pos_mm / sensel.getSensorWidthMM()  * WINDOW_WIDTH_PX);
      int screen_y = (int) (c[j].y_pos_mm / sensel.getSensorHeightMM() * WINDOW_HEIGHT_PX);
      if (b.is_hover_over(screen_x, screen_y))
      {
        temp_hover_over = true; 
        switch(c[j].type)
        {
        case SenselDevice.SENSEL_EVENT_CONTACT_START:
          if(b.type.equals("button") && b.value != "null")
          {
            println("Pressed " + b.value);
            r.keyPress(Integer.parseInt(b.value));
            println("pressed key");
          }
          break; 
        case SenselDevice.SENSEL_EVENT_CONTACT_MOVE:
          break; 
        case SenselDevice.SENSEL_EVENT_CONTACT_END:
          if(b.type.equals("button") && b.value != "null")
          {
            println("Pressed " + b.value);
            r.keyRelease(Integer.parseInt(b.value));
            println("pressed key");
          }
          break;
        default: 
          break;
        }
        break;
      }
    }
    b.hover_over = temp_hover_over || b.is_hover_over(mouseX, mouseY);
  }
}

//displays all elements in current Overlay
void DisplayOverlay()
{
  //println("Displaying Overlay");
  for (int i = 0; i < overlay.buttons.size (); ++i)
  {
    overlay.buttons.get(i).display();
  }
}

void draw() 
{
  if (!sensel_sensor_opened)
    return;

  background(0);
  textFont(font);

  SenselContact[] c = sensel.readContacts();

  //update(mouseX, mouseY);
  if (c == null)
  {
    println("NULL CONTACTS");
    return;
  }

  //update things
  UpdateOverlay(c);    //update the Overlay during edit

  //display things
  DisplayTouch(c);     //display the magic touch (user input)
  DisplayOverlay();   //display the buttons (the current design of the overlay)
  textFont(fontLarge);
  text(uInput,0,WINDOW_HEIGHT_PX-20);
  if (c.length > 0)
  {
    println("Selected: " + selected_buttons.size());
    println("User Input: " + uInput);
    println("****");
  }
}

void keyPressed() 
{
  if (keyCode == BACKSPACE) 
  {
    if (uInput.length() > 0) 
    {
      uInput = uInput.substring(0, uInput.length()-1);
    }
  } else if (keyCode == DELETE || keyCode == ENTER) 
  {
    uInput = "";
  } else if (keyCode != SHIFT && keyCode != CONTROL && keyCode != ALT) 
  {
    uInput += key;
  } 
}

public class DisposeHandler 
{   
  DisposeHandler(PApplet pa)
  {
    pa.registerMethod("dispose", this);
  }  
  public void dispose()
  {
    println("Closing sketch");
    if (sensel_sensor_opened)
    {
      sensel.stopScanning();
      sensel.closeConnection();
    }
  }
}

