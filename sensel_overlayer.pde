/**
 * Read Contacts
 * by Aaron Zarraga - Sensel, Inc
 * 
 * This opens a Sensel sensor, reads contact data, and prints the data to the console.
 */

import java.util.Map;
import java.io.*;
import java.nio.file.Files;
//import java.nio.file.StandardCopyOption;
//import java.nio.file.StandardOpenOption;

//We will scale the height such that we get the same aspect ratio as the sensor
int WINDOW_WIDTH_PX = 1150;
int WINDOW_HEIGHT_PX = 600;
int OVERLAYER_WIDTH_PX = int (WINDOW_WIDTH_PX * 0.90);
int OVERLAYER_HEIGHT_PX = int (WINDOW_HEIGHT_PX * 0.90);

boolean sensel_sensor_opened = false;
String  uInput    = "";
HashMap<Integer, Button> selected_buttons;
ArrayList<Button> gui_buttons;

SenselDevice sensel;
Overlay overlay; 
MODE mode;
HashMap<String, MODE> name_to_mode;
Coor siz_start_coor; 

PFont font;

void setup() 
{
  selected_buttons = new HashMap<Integer, Button>();
  gui_buttons = new ArrayList<Button>();
  gui_buttons.add(new Button("delete", OVERLAYER_WIDTH_PX + 10, OVERLAYER_HEIGHT_PX-30));
  gui_buttons.add(new Button("insert", OVERLAYER_WIDTH_PX + 10, int(OVERLAYER_HEIGHT_PX-default_height*1.5-30)));
  gui_buttons.add(new Button("edit", OVERLAYER_WIDTH_PX + 10, int(OVERLAYER_HEIGHT_PX-2*default_height*1.5-30)));
  gui_buttons.add(new Button("move", OVERLAYER_WIDTH_PX + 10, int(OVERLAYER_HEIGHT_PX-3*default_height*1.5-30)));
  gui_buttons.add(new Button("resize", OVERLAYER_WIDTH_PX + 10, int(OVERLAYER_HEIGHT_PX-4*default_height*1.5-30)));
  overlay = new Overlay(); 
  File file = new File("Documents/git/sensel_overlayer/view_overlay/overlays/overlay1");
  overlay.loadOverlay(file); 
  
  mode = MODE.NON;
  name_to_mode = new HashMap<String, MODE>();
  name_to_mode.put("delete", MODE.DEL);
  name_to_mode.put("insert", MODE.INS);
  name_to_mode.put("edit",   MODE.EDI);
  name_to_mode.put("move",   MODE.MOV);
  name_to_mode.put("resize", MODE.SIZ);
  name_to_mode.put("none",   MODE.NON);
  siz_start_coor = new Coor();

  font = createFont("Arial", 12, true);
  
  size(1150, 600);
  
  DisposeHandler dh = new DisposeHandler(this);
  sensel = new SenselDevice(this);
  sensel_sensor_opened = sensel.openConnection();
  if (!sensel_sensor_opened)
  {
    println("Unable to open Sensel sensor!");
    exit();
    return;
  }

  //Init window height so that display window aspect ratio matches sensor.
  //NOTE: This must be done AFTER senselInit() is called, because senselInit() initializes
  //  the sensor height/width fields. This dependency needs to be fixed in later revisions 
  //WINDOW_HEIGHT_PX = (int) (sensel.getSensorHeightMM() / sensel.getSensorWidthMM() * WINDOW_WIDTH_PX);

  //Enable contact sending
  sensel.setFrameContentControl(SenselDevice.SENSEL_FRAME_CONTACTS_FLAG);

  //Enable scanning
  sensel.startScanning();
}

//show the magic touch
void DisplayTouch(SenselContact[] c)
{
  //show sensor inputs
  for (int i = 0; i < c.length; i++)
  {
    int force = c[i].total_force;

    float sensor_x_mm = c[i].x_pos_mm;
    float sensor_y_mm = c[i].y_pos_mm;

    int screen_x = (int) (c[i].x_pos_mm / sensel.getSensorWidthMM()  * WINDOW_WIDTH_PX);
    int screen_y = (int) (c[i].y_pos_mm / sensel.getSensorHeightMM() * WINDOW_HEIGHT_PX);

    int id = c[i].id;
    int event_type = c[i].type;

    Button b; 
    String event;
    switch (event_type)
    {
      case SenselDevice.SENSEL_EVENT_CONTACT_INVALID:
        event = "invalid"; 
        break;
      case SenselDevice.SENSEL_EVENT_CONTACT_START:
        event = "start";
        break;
      case SenselDevice.SENSEL_EVENT_CONTACT_MOVE:
        event = "move";
        b = selected_buttons.get(c[i].id); 
        if (b!=null && mode == MODE.MOV)
        {
  //        println("MOVING");
          b.set_coor(screen_x, screen_y);
        }
        if (mousePressed && b!=null && mode == MODE.SIZ && (siz_start_coor.x > 0 || siz_start_coor.y > 0))
        {
          b.width = screen_x-siz_start_coor.x;
          b.height = screen_y-siz_start_coor.y;
        }
        break;
      case SenselDevice.SENSEL_EVENT_CONTACT_END:
        event = "end";
        if (selected_buttons.isEmpty() && selected_buttons.get(c[i].id)==null
          && screen_x <= OVERLAYER_WIDTH_PX && screen_y <= OVERLAYER_HEIGHT_PX && mode == MODE.INS)
        {
          b = new Button("Butt"+overlay.buttons.size(), screen_x, screen_y);
          b.set_coor(screen_x, screen_y);
          overlay.addButton(b);
        }
        if (selected_buttons.get(c[i].id) != null)
        {
          selected_buttons.get(c[i].id).sid = -1; 
          selected_buttons.remove(c[i].id);
        }
        break;
      default:
        event = "error";
    }

    println("Contact ID " + id + ", event=" + event + ", mm coord: (" + sensor_x_mm + ", " + sensor_y_mm + "), force=" + force); 

    int size = force / 100;
    if (size < 10) {size = 10;}

    //update(screen_x, screen_y);
    ellipse(screen_x, screen_y, size, size);
  }
}

void UpdateOverlay(SenselContact[] c) {
  //println("Displaying Overlay");
  for (int i = 0; i < overlay.buttons.size (); ++i)
  {
    Button b = overlay.buttons.get(i);
    boolean temp_hover_over = false;
    for (int j = 0; j < c.length; ++j)
    {
      int screen_x = (int) (c[j].x_pos_mm / sensel.getSensorWidthMM()  * WINDOW_WIDTH_PX);
      int screen_y = (int) (c[j].y_pos_mm / sensel.getSensorHeightMM() * WINDOW_HEIGHT_PX);
      if (b.is_hover_over(screen_x, screen_y))
      {
        temp_hover_over = true; 
        switch(c[j].type)
        {
          case SenselDevice.SENSEL_EVENT_CONTACT_START:
            if (b.sid < 0 && selected_buttons.get(c[j].id)==null)
            {
  //            println("SELECTING START");
              b.sid = c[j].id;
              selected_buttons.put(c[j].id, b);
            }
            break; 
          case SenselDevice.SENSEL_EVENT_CONTACT_MOVE:
            if (b.sid < 0 && selected_buttons.get(c[j].id)==null)
            {
  //            println("SELECTING MOVE");
              b.sid = c[j].id;
              selected_buttons.put(c[j].id, b);
            }
            break; 
          case SenselDevice.SENSEL_EVENT_CONTACT_END:
            b.sid = -1;
            if (selected_buttons.get(c[j].id) != null)
            {
              selected_buttons.get(c[j].id).sid = -1;
              selected_buttons.remove(c[j].id);
            }
            break; 
          default: 
            break;
        }
        break;
      }
    }
    b.hover_over = temp_hover_over || b.is_hover_over(mouseX, mouseY);
  }
  if (mode == MODE.DEL) DeleteSelected();
}

//displays all buttons/elements in current Overlay
void DisplayOverlay()
{
  for (int i = 0; i < overlay.buttons.size (); ++i)
  {
    overlay.buttons.get(i).display();
  }
}

void DeleteSelected() //deletes all the buttons in selected_buttons from the overlay
{
  for (int i = 0; i < selected_buttons.size (); ++i)
  {
    Button b = selected_buttons.get(i);
    if (overlay.buttons.contains(b))
    {
      overlay.buttons.remove(b);
    }
  }
  
  selected_buttons.clear();
}

void UpdateUI()
{
  line(OVERLAYER_WIDTH_PX,0,OVERLAYER_WIDTH_PX,OVERLAYER_HEIGHT_PX);
  line(0,OVERLAYER_HEIGHT_PX,OVERLAYER_WIDTH_PX,OVERLAYER_HEIGHT_PX);
  for (int i = 0; i < gui_buttons.size (); ++i)
  {
    Button b = gui_buttons.get(i);
    MODE new_mode = mode; 

    if (b.is_hover_over(mouseX, mouseY) && mousePressed)
    {
      new_mode = name_to_mode.get(b.name); 
//      siz_start_coor = new Coor();
      switch(new_mode)
      {
        case SIZ: 
//          if (selected_buttons.size() > 0)
//          {
//          }
          if (new_mode != mode && selected_buttons.keySet().size()>0 && selected_buttons.get(selected_buttons.keySet().toArray()[0])!=null)
          {
            siz_start_coor = selected_buttons.get(selected_buttons.keySet().toArray()[0]).coor;
          }

          break; 
        default: 
          siz_start_coor = new Coor();
          break; 
      }
      
    }
    mode = new_mode; 
    b.hover_over = (name_to_mode.get(b.name) == mode);
    if (mode == MODE.SIZ && !mousePressed)
    {
      mode = MODE.NON;
    }  
}
  
}

void DisplayUI()
{
  for (int i = 0; i < gui_buttons.size (); ++i)
  {
    Button b = gui_buttons.get(i);
//    b.hover_over = (name_to_mode.get(b.name) == mode);
    b.display();
  }
}

void draw() 
{
  if (!sensel_sensor_opened)
    return;

  background(0);
  textFont(font);

  SenselContact[] c = sensel.readContacts();

  //update(mouseX, mouseY);
  if (c == null)
  {
    println("NULL CONTACTS");
    return;
  }

  //update things
  UpdateOverlay(c);    //update the Overlay during edit
  UpdateUI();

  //display things
  DisplayTouch(c);     //display the magic touch (user input)
  DisplayOverlay();   //display the buttons (the current design of the overlay)
  DisplayUI();

  
  println("Current State: " + mode + "\tsiz_start_coor: " + siz_start_coor.x + "," + siz_start_coor.y);
  if (c.length > 0)
  {
    println("Selected: " + selected_buttons.size());
    println("User Input: " + uInput);
    println("****");
  }
}

void keyPressed() 
{
  
  if (keyCode == BACKSPACE) 
  {
    if (uInput.length() > 0) 
    {
      uInput = uInput.substring(0, uInput.length()-1);
    }
  } else if (keyCode == DELETE) 
  {
    uInput = "";
  } else if (keyCode == ENTER)
  {
    if(mode == MODE.EDI && selected_buttons.size() > 0) 
    {
      println("Setting " + selected_buttons.get(0).name + " to " + uInput);
      selected_buttons.get(0).set_val(uInput);
      uInput = "";
    }
  } else if (keyCode != CONTROL && keyCode != ALT) 
  {
    uInput = Integer.toString(keyCode);
  } 
}

public void createOverlayFile()
{
  try
  {
    //create "overlays" directory if it doesn't exist yet 
    File dirOverlay = new File("Documents/git/sensel_overlayer/view_overlay/overlays");
    if (!dirOverlay.exists())
    {
      dirOverlay.mkdirs();
    }
    
    File destOverlay = new File("Documents/git/sensel_overlayer/view_overlay/overlays/overlay1");
    if (!destOverlay.exists())
    {
      destOverlay.createNewFile();
    }
    
    String to_insert = "b.name\tb.type\tb.value\tb.coor.x\tb.coor.y\tb.width\tb.height\t\n";
    for (int i = 0; i < overlay.buttons.size (); ++i)
    {
      Button b = overlay.buttons.get(i);
      String s = (b.name+"\t"+b.type+"\t"+b.value+"\t"+b.coor.x+"\t"+b.coor.y+"\t"+b.width+"\t"+b.height+"\t\n");
      to_insert += s;
    }
    println(to_insert);
    Files.write(destOverlay.toPath(), to_insert.getBytes());
  }
  catch (IOException e) {
  };

  //FileWriter fw = new FileWriter(newOverlay);
}

public class DisposeHandler 
{   
  DisposeHandler(PApplet pa)
  {
    pa.registerMethod("dispose", this);
  }  
  public void dispose()
  {
    println("Closing sketch");
    if (sensel_sensor_opened)
    {
      sensel.stopScanning();
      sensel.closeConnection();
    }
    createOverlayFile();
    println("Created Overlay");
  }
}

